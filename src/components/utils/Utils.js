export default class Utils{

  static getAge(dateString)
  {
    var birthDate = new Date(dateString);

    // Relative to kemp start
    var forday = new Date("2019-07-06");

    // Relative to kemp end
    // var forday = new Date("2019-07-13");

    var age = forday.getFullYear() - birthDate.getFullYear();
    var m = forday.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && forday.getDate() < birthDate.getDate()))
    {
      age--;
    }

    return age;
  }
}

